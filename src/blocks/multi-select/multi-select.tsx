import { ReactNode } from "react";
import { filter, findFirst, map, tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { MultiSelect } from "@tripetto/block-multi-select/runner";
import { IChatRenderProps, IChatRendering } from "@interfaces/block";
import { MultiSelectFabric } from "@tripetto/runner-fabric/components/multi-select";
import { ButtonFabric } from "@tripetto/runner-fabric/components/button";
import { SubmitIcon } from "@ui/icons/submit";
import { NothingAnswered } from "@ui/message/nothing";
import { CheckboxAnswer } from "@ui/message/checkbox";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-multi-select",
})
export class MultiSelectBlock extends MultiSelect implements IChatRendering {
    answer(props: IChatRenderProps): ReactNode {
        const options = this.options<JSX.Element>(props);

        if (findFirst(options, (option) => option.value?.hasValue || false)) {
            const selected = map(
                filter(options, (checkbox) => checkbox.value?.value || false),
                (checkbox) => (
                    <CheckboxAnswer key={checkbox.id} checked={true} color={props.styles.answers.textColor}>
                        {checkbox.label}
                    </CheckboxAnswer>
                )
            );

            return (
                (selected.length > 0 && selected) || (
                    <NothingAnswered
                        label={props.l10n.pgettext("runner#2|💬 Messages|Nothing selected", "Nothing selected")}
                        color={props.styles.answers.textColor}
                    />
                )
            );
        }

        return undefined;
    }

    input(props: IChatRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                <MultiSelectFabric
                    id={props.id}
                    overlay={props.overlay}
                    styles={props.styles.inputs}
                    options={this.options(props)}
                    required={this.required}
                    error={props.isFailed}
                    placeholder={props.placeholder}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
                {props.ariaDescription}
                <ButtonFabric styles={props.styles.buttons} icon={SubmitIcon} label={props.submitLabel} onClick={done} />
            </>
        );
    }
}
