import { ReactNode } from "react";
import { filter, findFirst, map, tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Ranking } from "@tripetto/block-ranking/runner";
import { IChatRenderProps, IChatRendering } from "@interfaces/block";
import { RankingFabric } from "@tripetto/runner-fabric/components/ranking";
import { ButtonFabric } from "@tripetto/runner-fabric/components/button";
import { SubmitIcon } from "@ui/icons/submit";
import { CheckboxAnswer } from "@ui/message/checkbox";

@tripetto({
    namespace,
    type: "node",
    identifier: "@tripetto/block-ranking",
})
export class RankingBlock extends Ranking implements IChatRendering {
    answer(props: IChatRenderProps): ReactNode {
        const slots = this.slots;
        const options = this.options(props);
        const ranking = filter(
            map(
                filter(slots, (slot) => (slot.reference ? true : false)),
                (slot) => {
                    const option = findFirst(options, (o) => o.id === slot.reference);

                    if (option) {
                        return (
                            <CheckboxAnswer key={option.id} checked={true} color={props.styles.answers.textColor}>
                                {option.label}
                            </CheckboxAnswer>
                        );
                    }

                    return undefined;
                }
            ),
            (option) => (option ? true : false)
        );

        return (ranking.length > 0 && ranking) || undefined;
    }

    input(props: IChatRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                <RankingFabric
                    styles={props.styles.multipleChoice}
                    slots={this.slots}
                    options={this.options(props)}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
                {props.ariaDescription}
                <ButtonFabric styles={props.styles.buttons} icon={SubmitIcon} label={props.submitLabel} onClick={done} />
            </>
        );
    }
}
