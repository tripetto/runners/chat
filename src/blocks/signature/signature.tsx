import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Signature } from "@tripetto/block-signature/runner";
import { IChatRenderProps, IChatRendering } from "@interfaces/block";
import { SignatureFabric, SignatureViewFabric } from "@tripetto/runner-fabric/components/signature";
import { ButtonFabric } from "@tripetto/runner-fabric/components/button";
import { SubmitIcon } from "@ui/icons/submit";
import { TypingIndicator } from "@ui/typing-indicator";
import { ImageAnswer } from "@ui/message/image";

@tripetto({
    namespace,
    type: "node",
    identifier: "@tripetto/block-signature",
})
export class SignatureBlock extends Signature implements IChatRendering {
    answer(props: IChatRenderProps): ReactNode {
        return (
            <SignatureViewFabric
                controller={this}
                service={props.attachments}
                host={ImageAnswer}
                loading={<TypingIndicator color={props.styles.answers.textColor} animate={true} />}
                error={this.signatureSlot.string || undefined}
            />
        );
    }

    input(props: IChatRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                <SignatureFabric
                    styles={props.styles.fileUpload}
                    l10n={props.l10n}
                    controller={this}
                    service={props.attachments}
                    size={this.size}
                    color={this.color}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                    labels={(id, message) => {
                        switch (id) {
                            case "processing":
                                return props.l10n.pgettext("runner#6|✍️ Signature", "Processing signature (%1)", message);
                            case "signed":
                                return props.l10n.pgettext("runner#6|✍️ Signature", "Signed on %1", message);
                            case "clear":
                                return props.l10n.pgettext("runner#6|✍️ Signature", "Clear");
                        }
                    }}
                />
                {props.ariaDescription}
                <ButtonFabric
                    styles={props.styles.buttons}
                    icon={SubmitIcon}
                    label={props.submitLabel}
                    disabled={!this.signatureSlot.hasValue}
                    onClick={done}
                />
            </>
        );
    }
}
