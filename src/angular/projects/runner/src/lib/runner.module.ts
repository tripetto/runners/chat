import { NgModule } from "@angular/core";
import { TripettoChatComponent } from "./runner.component";

@NgModule({
    declarations: [TripettoChatComponent],
    imports: [],
    exports: [TripettoChatComponent],
})
export class TripettoChatModule {}
