import { styled } from "styled-components";
import { ReactNode } from "react";
import { CheckedIcon } from "@ui/icons/checked";
import { UncheckedIcon } from "@ui/icons/unchecked";
import { MESSAGE_SIZE, SIZE } from "../../const";

export const CheckboxAnswerElement = styled.div<{
    $color: string;
    $children: ReactNode;
}>`
    min-height: ${MESSAGE_SIZE / SIZE}em;
    padding: ${8 / SIZE}em ${16 / SIZE}em;
    display: flex;

    span {
        color: ${(props) => props.$color};
        align-self: center;
    }

    svg {
        min-width: ${24 / SIZE}em;
        width: ${24 / SIZE}em;
        height: ${24 / SIZE}em;
        margin-right: ${(props) => (props.$children && `${8 / SIZE}em`) || undefined};
        fill: ${(props) => props.$color};
    }
`;

export const CheckboxAnswer = (props: { readonly checked: boolean; readonly children: ReactNode; readonly color: string }) => (
    <CheckboxAnswerElement $color={props.color} $children={props.children}>
        {props.checked ? CheckedIcon : UncheckedIcon}
        <span>{props.children}</span>
    </CheckboxAnswerElement>
);
