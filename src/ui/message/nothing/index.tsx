import { styled } from "styled-components";
import { NothingIcon } from "@ui/icons/nothing";
import { MESSAGE_SIZE, SIZE } from "../../const";

export const NothingAnsweredElement = styled.div<{
    $color: string;
    $label: string;
}>`
    min-height: ${MESSAGE_SIZE / SIZE}em;
    padding: ${8 / SIZE}em ${16 / SIZE}em;
    display: flex;

    span {
        color: ${(props) => props.$color};
        align-self: center;
    }

    svg {
        min-width: ${24 / SIZE}em;
        width: ${24 / SIZE}em;
        height: ${24 / SIZE}em;
        margin-right: ${(props) => (props.$label && `${8 / SIZE}em`) || undefined};
        fill: ${(props) => props.$color};
    }
`;

export const NothingAnswered = (props: { readonly label: string; readonly color: string }) => (
    <NothingAnsweredElement $color={props.color} $label={props.label}>
        {NothingIcon}
        <span>{props.label}</span>
    </NothingAnsweredElement>
);
