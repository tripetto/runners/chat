import { styled } from "styled-components";
import { ReactNode } from "react";
import { SIZE } from "../const";

const MessageContainerElement = styled.div<{
    $alignment: "left" | "right";
    $disabled?: boolean;
    $spacing?: boolean;
}>`
    display: flex;
    justify-content: ${(props) => (props.$alignment === "left" ? "flex-start" : "flex-end")};
    align-items: flex-start;
    width: 100%;
    opacity: ${(props) => (props.$disabled && "0.2") || undefined};
    transition: opacity 0.2s ease-out;
    padding-top: ${(props) => props.$spacing && `${13 / SIZE}em;`};

    &:hover {
        opacity: 1;
    }
`;

export const MessageContainer = (props: {
    readonly alignment: "left" | "right";
    readonly measure?: (el: HTMLElement | null) => void;
    readonly disabled?: boolean;
    readonly spacing?: boolean;
    readonly children?: ReactNode;
}) => (
    <MessageContainerElement $alignment={props.alignment} $disabled={props.disabled} $spacing={props.spacing} ref={props.measure}>
        {props.children}
    </MessageContainerElement>
);
