import { styled } from "styled-components";
import { ReactNode } from "react";
import { MESSAGE_SIZE, SIZE } from "../../const";

export const MatrixAnswerElement = styled.div<{
    $color: string;
}>`
    min-height: ${MESSAGE_SIZE / SIZE}em;
    padding: ${8 / SIZE}em ${16 / SIZE}em;
    display: flex;
    align-items: center;
    color: ${(props) => props.$color};
`;

export const MatrixAnswer = (props: { readonly children: ReactNode; readonly color: string }) => (
    <MatrixAnswerElement $color={props.color}>{props.children}</MatrixAnswerElement>
);
