import { styled, RuleSet } from "styled-components";
import { ReactNode } from "react";
import { isNumberFinite } from "@tripetto/runner";
import { AvatarAnimationDuration } from "@ui/avatar/animation";
import { color } from "@tripetto/runner-fabric/color";
import { MESSAGE_SIZE, SIZE } from "../const";

const MessageElement = styled.div<{
    $alignment: "left" | "right";
    $backgroundColor?: string;
    $borderColor?: string;
    $roundness?: number;
    $textColor?: string;
    $animation?: RuleSet<object> | false;
    $isFirst?: boolean;
    $isLast?: boolean;
    $justify?: boolean;
}>`
    width: ${(props) => props.$justify && "100%"};
    max-width: ${(props) => !props.$justify && "75%"};
    min-width: ${MESSAGE_SIZE / SIZE}em;
    min-height: ${MESSAGE_SIZE / SIZE}em;
    border-radius: ${(props) => (isNumberFinite(props.$roundness) ? `${props.$roundness}px` : `${20 / SIZE}em`)};
    border-bottom-left-radius: ${(props) => props.$alignment === "left" && (typeof props.$isLast !== "boolean" || props.$isLast) && "0"};
    border-bottom-right-radius: ${(props) => props.$alignment === "right" && (typeof props.$isLast !== "boolean" || props.$isLast) && "0"};
    transition: border-radius ${AvatarAnimationDuration / 1000}s ease-out;
    transform-origin: ${(props) => props.$alignment} calc(100% - ${MESSAGE_SIZE / SIZE / 2}em);
    background-color: ${(props) => props.$backgroundColor};
    border: ${(props) => props.$borderColor && `1px solid ${props.$borderColor}`};
    color: ${(props) => props.$textColor};
    overflow: hidden;
    cursor: default !important;
    z-index: 1;

    a {
        color: ${(props) => props.$textColor};
        text-decoration: underline;

        &:hover {
            color: ${(props) => props.$textColor && color(props.$textColor, (o) => o.manipulate((m) => m.lighten(0.3)))};
        }
    }

    ${(props) => props.$animation || ""}
`;

export const Message = (props: {
    readonly alignment: "left" | "right";
    readonly roundness?: number;
    readonly backgroundColor?: string;
    readonly borderColor?: string;
    readonly textColor?: string;
    readonly animation?: RuleSet<object> | false;
    readonly isFirst?: boolean;
    readonly isLast?: boolean;
    readonly justify?: boolean;
    readonly onWait?: () => void;
    readonly onContinue?: () => void;
    readonly onClick?: () => void;
    readonly children?: ReactNode;
}) => {
    return (
        <MessageElement
            onAnimationStart={props.onWait}
            onAnimationEnd={props.onContinue}
            onClick={props.onClick}
            $alignment={props.alignment}
            $backgroundColor={props.backgroundColor}
            $borderColor={props.borderColor}
            $roundness={props.roundness}
            $textColor={props.textColor}
            $animation={props.animation}
            $isFirst={props.isFirst}
            $isLast={props.isLast}
            $justify={props.justify}
        >
            {props.children}
        </MessageElement>
    );
};
