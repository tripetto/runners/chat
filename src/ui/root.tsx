import { styled, StyleSheetManager, createGlobalStyle, css, keyframes } from "styled-components";
import { CSSProperties, MutableRefObject, ReactNode, useEffect, useRef, useState } from "react";
import { Num, Str, castToBoolean } from "@tripetto/runner";
import { TRunnerViews } from "@tripetto/runner-react-hook";
import { color } from "@tripetto/runner-fabric/color";
import { TChatDisplay } from "@interfaces/props";
import { Frame, FrameConfig } from "@hooks/frame";
import { IRuntimeStyles } from "@hooks/styles";
import { scrollEffect } from "@helpers/scroll";
import { attach, detach } from "@helpers/resizer";
import { AfterInputAnimationDuration } from "@ui/input/animation";
import { MARGIN_BUTTON, MARGIN_MESSAGES, MARGIN_PAGE, MAX_HEIGHT, OFFSET, SMALL_SCREEN_MARGIN, SMALL_SCREEN_SIZE } from "./const";

const RootActivateAnimation = css`
    animation: ${keyframes`
        from {
            opacity: 0;
            transform: translateY(50px) scale(.5);
        }
        to {
            opacity: 1;
            transform: translateY(0) scale(1);

        }
    `} 0.25s ease-out;
`;

const RootDeactivateAnimation = css`
    animation: ${keyframes`
        from {
            opacity: 1;
            transform: translateY(0) scale(1);
        }
        to {
            opacity: 0;
            transform: translateY(50px) scale(.5);
        }
    `} 0.25s ease-out;
`;

const RootFrame = styled(Frame).withConfig(FrameConfig)<{
    $view: TRunnerViews;
    $display: TChatDisplay;
    $styles: IRuntimeStyles;
    $isVisible: boolean;
    $useAnimation: boolean;
}>`
    background-color: transparent !important;
    border: none !important;
    position: ${(props) =>
        props.$display === "button"
            ? props.$view === "live"
                ? "fixed"
                : "absolute"
            : props.$display === "inline" || props.$view !== "live"
              ? "relative"
              : "fixed"};
    left: ${(props) =>
        (props.$display === "inline" && props.$view === "live" && `-${OFFSET}px`) ||
        (props.$display === "page" && props.$view === "live" && "0") ||
        undefined};
    top: ${(props) =>
        (props.$display === "inline" && props.$view === "live" && `-${OFFSET}px`) ||
        (props.$display === "page" && props.$view === "live" && "0") ||
        undefined};
    right: ${(props) => (props.$display === "button" && "32px") || undefined};
    bottom: ${(props) => (props.$display === "button" && "128px") || undefined};
    width: ${(props) =>
        props.$display === "button"
            ? "calc(100% - 64px)"
            : props.$display === "inline" && props.$view === "live"
              ? `calc(100% + ${OFFSET * 2}px);`
              : "100%"};
    height: ${(props) =>
        props.$display === "button" ? "calc(100% - 160px)" : props.$display === "inline" && props.$view === "live" ? "0" : "100%"};
    max-width: ${(props) => (!props.$styles.fullWidth && props.$display === "button" && "480px") || undefined};
    max-height: ${(props) =>
        (props.$display === "button" && props.$view === "live" && props.$styles.background.color !== "transparent" && MAX_HEIGHT + "px") ||
        undefined};
    margin-bottom: ${(props) => (props.$display === "inline" && props.$view === "live" && `-${OFFSET * 2}px`) || undefined};
    margin-right: ${(props) => (props.$display === "inline" && props.$view === "live" && `-${OFFSET * 2}px`) || undefined};
    box-shadow: ${(props) =>
        (props.$display === "button" &&
            props.$styles.background.color !== "transparent" &&
            `0 5px 20px 5px ${color(props.$styles.background.color, (o) => o.makeBlackOrWhite().manipulate((m) => m.alpha(0.2)))}`) ||
        undefined};
    border-radius: ${(props) => (props.$display === "button" && props.$styles.background.color !== "transparent" && "8px") || undefined};
    z-index: ${(props) => (props.$display !== "inline" ? 2147483645 : undefined)};
    transition:
        max-height 0.3s linear,
        opacity 0.3s,
        box-shadow 0.3s;
    transform-origin: bottom right;
    transform: ${(props) => (props.$display === "button" ? (props.$isVisible ? "scale(1)" : "scale(0)") : undefined)};
    opacity: ${(props) => (props.$display === "button" ? (props.$isVisible ? 1 : 0) : undefined)};
    pointer-events: ${(props) => (props.$display === "button" ? (props.$isVisible ? "auto" : "none") : undefined)};

    ${(props) =>
        props.$display === "button" && props.$useAnimation
            ? props.$isVisible
                ? RootActivateAnimation
                : RootDeactivateAnimation
            : undefined}

    @media (max-device-width: ${SMALL_SCREEN_SIZE}px) {
        bottom: ${(props) => (props.$display === "button" ? "88px" : undefined)};
        left: ${(props) => (props.$display === "button" ? 0 : undefined)};
        right: ${(props) => (props.$display === "button" ? 0 : undefined)};
        width: ${(props) => (props.$display === "button" ? "100%" : undefined)};
        height: ${(props) => (props.$display === "button" ? "calc(100% - 72px)" : undefined)};
        max-width: none;
        max-height: none;
        z-index: ${(props) => (props.$display !== "inline" ? 2147483647 : undefined)};
    }
`;

const RootBody = createGlobalStyle<{
    $customCSS?: string;
}>`
    body {
        overflow: hidden;
        background-color: transparent;

        ${(props) =>
            props.$customCSS &&
            css`
                ${Str.replace(props.$customCSS, "tripetto-block-", "@tripetto/block-")}
            `}
    }
`;

const RootElement = styled.div<{
    $view: TRunnerViews;
    $display: TChatDisplay;
    $fontFamily: string;
    $styles: IRuntimeStyles;
}>`
    position: absolute;
    left: ${(props) => (props.$display === "inline" && props.$view === "live" ? `${OFFSET}px` : "0")};
    right: ${(props) => (props.$display === "inline" && props.$view === "live" ? `${OFFSET}px` : "0")};
    top: ${(props) => (props.$display === "inline" && props.$view === "live" ? `${OFFSET}px` : "0")};
    bottom: ${(props) => (props.$display === "inline" && props.$view === "live" ? `${OFFSET}px` : "0")};
    font-family: ${(props) => props.$fontFamily};
    font-size: ${(props) => props.$styles.font.size}px;
    font-variant-ligatures: none;
    background-color: ${(props) => color(props.$styles.background.color)};
    background-image: ${(props) =>
        (props.$styles.background.url &&
            props.$styles.background.opacity > 0 &&
            `${
                (props.$styles.background.opacity < 1 &&
                    `linear-gradient(${color(props.$styles.background.color, (o) =>
                        o.manipulate((m) => m.alpha(1 - props.$styles.background.opacity))
                    )},${color(props.$styles.background.color, (o) =>
                        o.manipulate((m) => m.alpha(1 - props.$styles.background.opacity))
                    )}),`) ||
                ""
            }url("${props.$styles.background.url}")`) ||
        undefined};
    background-size: ${(props) =>
        (props.$styles.background.url && props.$styles.background.positioning !== "repeat" && props.$styles.background.positioning) ||
        undefined};
    background-repeat: ${(props) =>
        props.$styles.background.url && props.$styles.background.positioning === "repeat" ? "repeat" : "no-repeat"};
    background-position: center center;
    line-height: 1.5em;
    overflow-x: ${(props) => (props.$display === "inline" && props.$view === "live" ? "visible" : "hidden")};
    overflow-y: ${(props) => (props.$display === "inline" && props.$view === "live" ? "visible" : "auto")};
    scroll-behavior: smooth;
    scrollbar-width: ${(props) =>
        (props.$view !== "preview" &&
            props.$display === "button" &&
            (!props.$styles.showScrollbar || props.$styles.background.color === "transparent") &&
            "none") ||
        undefined};
    -ms-overflow-style: ${(props) =>
        (props.$view !== "preview" &&
            props.$display === "button" &&
            (!props.$styles.showScrollbar || props.$styles.background.color === "transparent") &&
            "none") ||
        undefined};
    -webkit-overflow-scrolling: touch;
    -webkit-tap-highlight-color: transparent;

    &::-webkit-scrollbar {
        display: ${(props) =>
            (props.$view !== "preview" &&
                props.$display === "button" &&
                (!props.$styles.showScrollbar || props.$styles.background.color === "transparent") &&
                "none") ||
            undefined};
    }

    * {
        font-family: ${(props) => props.$fontFamily};
        font-variant-ligatures: none;
        box-sizing: border-box;
        outline: none;
    }

    > div {
        max-width: ${(props) => (!props.$styles.fullWidth && props.$display !== "button" && "800px") || undefined};
        margin-left: ${(props) => (!props.$styles.fullWidth && props.$display !== "button" && "auto") || undefined};
        margin-right: ${(props) => (!props.$styles.fullWidth && props.$display !== "button" && "auto") || undefined};
        padding: ${(props) =>
            props.$display === "button"
                ? `${MARGIN_BUTTON}px ${MARGIN_BUTTON}px ${props.$view === "preview" ? MARGIN_BUTTON : 0}px`
                : props.$display === "page" || props.$view !== "live"
                  ? `${MARGIN_PAGE}px ${MARGIN_PAGE}px ${props.$view === "preview" ? MARGIN_PAGE : 0}px`
                  : undefined};
    }

    > div > div > div + div {
        margin-top: ${MARGIN_MESSAGES}px;
    }

    @media (prefers-reduced-motion: reduce) {
        scroll-behavior: auto;
    }

    @media (${(props) => (props.$view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
        font-size: ${(props) => props.$styles.font.sizeSmall}px;

        > div {
            padding: ${(props) =>
                props.$display === "page" || (props.$display === "inline" && props.$view !== "live")
                    ? `${SMALL_SCREEN_MARGIN}px ${SMALL_SCREEN_MARGIN}px ${props.$view === "preview" ? SMALL_SCREEN_MARGIN : 0}px`
                    : undefined};
        }
    }
`;

const Content = (props: {
    readonly children?: ReactNode;
    readonly onChange: (height: number) => void;
    readonly resizeRef: MutableRefObject<(() => void) | undefined>;
}) => {
    const contentRef = useRef<HTMLElement>() as MutableRefObject<HTMLDivElement>;

    useEffect(() => {
        const ref = attach(
            (props.resizeRef.current = () => {
                if (contentRef.current) {
                    props.onChange(contentRef.current.getBoundingClientRect().height);
                }
            })
        );

        return () => detach(ref);
    });

    return <div ref={contentRef}>{props.children}</div>;
};

export const ChatRoot = (props: {
    readonly frameRef: MutableRefObject<HTMLIFrameElement>;
    readonly view: TRunnerViews;
    readonly display: TChatDisplay;
    readonly isVisible?: boolean;
    readonly title?: string;
    readonly styles: IRuntimeStyles;
    readonly className?: string;
    readonly customStyle?: CSSProperties;
    readonly customCSS?: string;
    readonly children?: ReactNode;
    readonly onTouch?: () => void;
}) => {
    const [contentHeight, setContentHeight] = useState(0);
    const [scrollHeight, setScrollHeight] = useState(0);
    const rootRef = useRef<HTMLDivElement>() as MutableRefObject<HTMLDivElement | null>;
    const resizeRef = useRef<() => void>();
    const cancelAnimationRef = useRef<() => void>();
    const isVisible = castToBoolean(props.isVisible, props.view !== "live");
    const useAnimation = typeof props.isVisible === "boolean";

    useEffect(() => {
        if (
            props.frameRef.current &&
            props.view === "live" &&
            (props.styles.autoFocus || props.display === "page" || (props.display === "button" && isVisible))
        ) {
            props.frameRef.current.focus();
        }

        if (
            props.view !== "preview" &&
            (props.display !== "inline" || props.view !== "live") &&
            (props.display !== "button" || isVisible) &&
            rootRef.current
        ) {
            const top = Num.max(contentHeight - rootRef.current.clientHeight, 0);

            scrollEffect(
                rootRef.current,
                top,
                rootRef.current.scrollTop > top ? 200 : 0,
                AfterInputAnimationDuration + 50,
                cancelAnimationRef
            ).then(() => setScrollHeight(contentHeight));
        } else if (props.display === "inline" && props.view === "live" && rootRef.current) {
            rootRef.current.scrollIntoView({
                behavior: "smooth",
                block: "end",
            });
        }
    }, [contentHeight, isVisible]);

    return (
        <RootFrame
            frameRef={props.frameRef}
            resizeRef={resizeRef}
            title={props.title}
            style={{
                height: (props.display === "inline" && props.view === "live" && contentHeight + OFFSET * 2 + "px") || undefined,
                maxHeight:
                    (props.view !== "preview" && props.display === "button" && Num.min(contentHeight, MAX_HEIGHT) + "px") || undefined,
                ...props.customStyle,
            }}
            font={props.styles.font && props.styles.font.family}
            className={props.className}
            onTouch={props.onTouch}
            $view={props.view}
            $display={props.display}
            $styles={props.styles}
            $isVisible={isVisible}
            $useAnimation={useAnimation}
        >
            {(doc: Document, fontFamily: string) => (
                <StyleSheetManager target={doc.head}>
                    <>
                        <RootBody $customCSS={props.customCSS} />
                        <RootElement
                            ref={rootRef}
                            $view={props.view}
                            $display={props.display}
                            $styles={props.styles}
                            $fontFamily={fontFamily}
                        >
                            <div
                                style={{
                                    minHeight:
                                        (props.view !== "preview" &&
                                            (props.display !== "inline" || props.view !== "live") &&
                                            scrollHeight + "px") ||
                                        undefined,
                                }}
                            >
                                <Content
                                    onChange={(height: number) => {
                                        height =
                                            Num.ceil(height) +
                                            (props.display === "button"
                                                ? MARGIN_BUTTON
                                                : props.display === "page" || props.view !== "live"
                                                  ? MARGIN_PAGE
                                                  : 0) *
                                                2;

                                        if (height !== contentHeight) {
                                            setContentHeight(height);

                                            if (height > scrollHeight) {
                                                setScrollHeight(height);
                                            }
                                        }
                                    }}
                                    resizeRef={resizeRef}
                                >
                                    {props.children}
                                </Content>
                            </div>
                        </RootElement>
                    </>
                </StyleSheetManager>
            )}
        </RootFrame>
    );
};
