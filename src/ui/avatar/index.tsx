import { styled, RuleSet } from "styled-components";
import { IAvataaars, avataaars } from "@tripetto/runner-fabric/components/avataaars";
import { MESSAGE_SIZE, SIZE } from "../const";

const AvatarElement = styled.div<{
    $aligment: "left" | "right";
    $visible: boolean;
    $backgroundColor?: string;
    $image: string | undefined;
    $animation: RuleSet<object> | undefined;
}>`
    min-width: ${MESSAGE_SIZE / SIZE}em;
    max-width: ${MESSAGE_SIZE / SIZE}em;
    min-height: ${MESSAGE_SIZE / SIZE}em;
    max-height: ${MESSAGE_SIZE / SIZE}em;
    margin-${(props) => (props.$aligment === "left" ? "right" : "left")}: 8px;
    border-radius: 50%;
    transform-origin: center center;
    background-color: ${(props) => props.$backgroundColor};
    display: flex;
    align-items: center;
    align-self: flex-end;
    justify-content: center;
    background-repeat: no-repeat;
    background-size: ${(props) => (props.$image ? `${MESSAGE_SIZE / SIZE}em ${MESSAGE_SIZE / SIZE}em` : undefined)};
    background-position: center center;
    background-image: ${(props) => props.$image && `url("${props.$image}");`};
    opacity: ${(props) => (props.$visible ? 1 : 0)};
    pointer-events: ${(props) => (props.$visible ? "auto" : "none")};
    transition: none !important;

    ${(props) => props.$animation || ""}
`;

export const Avatar = (props: {
    readonly alignment: "left" | "right";
    readonly backgroundColor?: string;
    readonly visible?: boolean;
    readonly animation?: RuleSet<object> | false;
    readonly onClick?: () => void;
    readonly avatar?: string | IAvataaars;
}) => {
    const visible = typeof props.visible !== "boolean" || props.visible;

    return (
        <AvatarElement
            onClick={() => visible && props.onClick && props.onClick()}
            $aligment={props.alignment}
            $backgroundColor={(props.avatar && props.backgroundColor) || undefined}
            $visible={visible}
            $image={
                (typeof props.avatar === "string" && props.avatar) ||
                (typeof props.avatar === "object" && props.avatar && `data:image/svg+xml;base64,${avataaars(props.avatar, "base64")}`) ||
                undefined
            }
            $animation={props.animation || undefined}
        />
    );
};
