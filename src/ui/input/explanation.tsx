import { styled } from "styled-components";
import { ReactNode } from "react";

export const ExplanationElement = styled.div<{
    $color?: string;
}>`
    width: 100%;
    color: ${(props) => props.$color};
    font-size: 0.8em;
    margin-top: 8px !important;
    line-height: 1.3em;

    a {
        color: ${(props) => props.$color};
        text-decoration: underline;

        &:hover {
            color: ${(props) => props.$color};
            text-decoration: underline;
        }
    }
`;

export const Explanation = (props: { readonly aria?: string; readonly color?: string; readonly children?: ReactNode }) => (
    <ExplanationElement id={props.aria} $color={props.color}>
        {props.children}
    </ExplanationElement>
);
